package ru.shchurin.tm;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.service.ProjectTaskService;
import ru.shchurin.tm.util.ConsoleUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class App {
    private static final String PROJECT_CLEAR = "PROJECT_CLEAR";
    private static final String PROJECT_CREATE = "PROJECT_CREATE";
    private static final String PROJECT_LIST = "PROJECT_LIST";
    private static final String PROJECT_REMOVE = "PROJECT_REMOVE";
    private static final String TASK_CLEAR = "TASK_CLEAR";
    private static final String TASK_CREATE = "TASK_CREATE";
    private static final String TASK_LIST = "TASK_LIST";
    private static final String TASK_REMOVE = "TASK_REMOVE";
    private static final String HELP = "HELP";
    private static final String EXIT = "EXIT";
    private static final String TASKS_OF_PROJECT = "TASKS_OF_PROJECT";
    private static final String PROJECT_ID = "PROJECT_ID";

    public static void main( String[] args ) throws IOException, ParseException {
        ProjectTaskService projectTaskService = new ProjectTaskService();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        boolean exit = true;
        while (exit) {
            String command = ConsoleUtil.getStringFromConsole();

            switch (command) {
                case PROJECT_CLEAR: projectTaskService.deleteAllProject();
                    break;
                case PROJECT_CREATE: projectTaskService.createProject();
                    break;
                case PROJECT_LIST: projectTaskService.showAllProject();
                    break;
                case PROJECT_REMOVE: projectTaskService.deleteProjectByName();
                    break;
                case TASK_CLEAR: projectTaskService.deleteAllTasks();
                    break;
                case TASK_CREATE: projectTaskService.createTask();
                    break;
                case TASK_LIST: projectTaskService.showAllTask();
                    break;
                case TASK_REMOVE: projectTaskService.deleteTaskByName();
                    break;
                case HELP: projectTaskService.showAllCommand();
                    break;
                case EXIT: exit = false;
                    break;
                case TASKS_OF_PROJECT: projectTaskService.showTasksOfProject();
                    break;
                case PROJECT_ID: projectTaskService.showProjectId();
                    break;
                default: projectTaskService.showMessageWrong();
                    break;
            }
        }
    }
}
