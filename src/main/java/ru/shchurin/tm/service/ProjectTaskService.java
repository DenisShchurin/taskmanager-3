package ru.shchurin.tm.service;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.util.DateUtil;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ProjectTaskService {
    private List<Project> projects = new ArrayList<>();
    private List<Task> tasks = new ArrayList<>();

    public ProjectTaskService() {
    }

    public void deleteAllProject() {
        projects.clear();
        System.out.println("[ALL PROJECT REMOVE]");
    }

    public void deleteAllTasks() {
        tasks.clear();
        System.out.println("[ALL TASK REMOVE]");
    }

    public void createProject() throws IOException, ParseException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER START_DATE:");
        String start = ConsoleUtil.getStringFromConsole();
        Date startDate = DateUtil.parseDate(start);
        System.out.println("ENTER END_DATE:");
        String end = ConsoleUtil.getStringFromConsole();
        Date endDate = DateUtil.parseDate(end);
        projects.add(new Project(name, startDate, endDate));
        System.out.println("[OK]");
    }

    public void createTask() throws IOException, ParseException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER PROJECT_ID:");
        String id = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER START_DATE:");
        String start = ConsoleUtil.getStringFromConsole();
        Date startDate = DateUtil.parseDate(start);
        System.out.println("ENTER END_DATE:");
        String end = ConsoleUtil.getStringFromConsole();
        Date endDate = DateUtil.parseDate(end);
        tasks.add(new Task(name, id, startDate, endDate));
        System.out.println("[OK]");
    }

    public void showProjectId() throws IOException {
        System.out.println("ENTER NAME:");
        String name = ConsoleUtil.getStringFromConsole();
        for (Project project : projects) {
            if (project.getName().equals(name)) {
                System.out.println(project.getId());
            }
        }
    }

    public void showAllProject() {
        System.out.println("[PROJECT LIST]");
        for (int i = 0; i < projects.size(); i++) {
            int number = i + 1;
            System.out.println(number + ". " + projects.get(i).getName() + ", " + DateUtil.dateFormat(projects.get(i).getStartDate()));
        }
    }

    public void showAllTask() {
        System.out.println("[TASK LIST]");
        for (int i = 0; i < tasks.size(); i++) {
            int number = i + 1;
            System.out.println(number + ". " + tasks.get(i).getName() + ", " + DateUtil.dateFormat(tasks.get(i).getStartDate()));
        }
    }

    public void showAllCommand() {
        System.out.println("HELP: Show all commands.");
        System.out.println("PROJECT_CLEAR: Remove all projects.");
        System.out.println("PROJECT_CREATE: Create new projects.");
        System.out.println("PROJECT_LIST: Show all projects.");
        System.out.println("PROJECT_REMOVE: Remove selected project");
        System.out.println("TASK_CLEAR: Remove all task.");
        System.out.println("TASK_CREATE: Create new task.");
        System.out.println("TASK_LIST: Show all tasks.");
        System.out.println("TASK_REMOVE: Remove selected task.");
        System.out.println("TASKS_OF_PROJECT: Show all tasks of project.");
        System.out.println("PROJECT_ID: Show id of project.");
    }

    public void deleteProjectByName() throws IOException {
        List<String> projectNames = new ArrayList<>();
        String projectId = "";

        for (Project project : projects) {
            projectNames.add(project.getName());
        }

        System.out.println("ENTER PROJECT NAME:");
        String name = ConsoleUtil.getStringFromConsole();

        if (projectNames.contains(name)) {
            for (Project project : projects) {
                if (project.getName().equals(name)) {
                    projectId = project.getId();
                }
            }

            for (Iterator<Project> it = projects.iterator(); it.hasNext(); ) {
                if (it.next().getName().equals(name)) {
                    it.remove();
                }
            }

            for (Iterator<Task> it = tasks.iterator(); it.hasNext(); ) {
                if (it.next().getProjectId().equals(projectId)) {
                    it.remove();
                }
            }
        } else {
            System.out.println("YOU ENTERED WRONG PROJECT NAME");
        }
        System.out.println("[PROJECT AND HIS TASKS REMOVE]");
    }

    public void deleteTaskByName() throws IOException {
        List<String> taskNames = new ArrayList<>();

        for (Task task : tasks) {
            taskNames.add(task.getName());
        }

        System.out.println("ENTER TASK NAME");
        String name = ConsoleUtil.getStringFromConsole();
        if (taskNames.contains(name)) {
            for (Iterator<Task> it = tasks.iterator(); it.hasNext(); ) {
                if (it.next().getName().equals(name)) {
                    it.remove();
                }
            }
        } else {
            System.out.println("YOU ENTERED WRONG TASK NAME");
        }
        System.out.println("[TASK REMOVE]");
    }

    public void showMessageWrong() {
        System.out.println("YOU ENTERED WRONG COMMAND");
    }

    public void showTasksOfProject() throws IOException {
        System.out.println("[TASKS OF PROJECT]");
        List<String> projectNames = new ArrayList<>();
        String projectId = "";

        for (Project project : projects) {
            projectNames.add(project.getName());
        }

        System.out.println("ENTER PROJECT NAME");
        String name = ConsoleUtil.getStringFromConsole();

        if (projectNames.contains(name)) {
            for (Project project : projects) {
                if (project.getName().equals(name)) {
                    projectId = project.getId();
                }
            }

            for (int i = 0; i < tasks.size(); i++) {
                int number = i + 1;
                if (tasks.get(i).getProjectId().equals(projectId)) {
                    System.out.println(number + ". " + tasks.get(i).getName() + ", " + DateUtil.dateFormat(tasks.get(i).getStartDate()));
                }
            }
        } else {
            System.out.println("YOU ENTERED WRONG PROJECT NAME");
        }
    }


    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
